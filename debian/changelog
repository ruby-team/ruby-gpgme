ruby-gpgme (2.0.23-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Vinay Keshava <vinaykeshava@disroot.org>  Sun, 10 Dec 2023 22:57:09 +0530

ruby-gpgme (2.0.22-1) unstable; urgency=medium

  * New upstream version 2.0.22:
    - Ships fix to prefer pkg-config over gpgme-config. (Closes: #1024651)
  * debian/control:
    - Build-Depend on pkg-config.
    - Drop obsolete field XB-Ruby-Versions.
    - Drop obsolete dependency on ruby-interpreter.
  * debian/patches:
    - Refresh for new release.

 -- Georg Faerber <georg@debian.org>  Fri, 25 Nov 2022 12:57:58 +0000

ruby-gpgme (2.0.20-1) unstable; urgency=medium

  * Team Upload
  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Gunnar Wolf ]
  * Removing myself from uploaders, acknowledging reality

  [ Micah Anderson ]
  * Remove myself from debian/control Uploaders field

  [ Ajayi Olatunji ]
  * New upstream Release
  * New upstream version 2.0.20
  * d/control (Standards-Version): Bump to 4.6.1.

 -- Ajayi Olatunji <ajayiolatunji15@yahoo.com>  Sun, 14 Aug 2022 00:05:46 +0000

ruby-gpgme (2.0.19-3) unstable; urgency=medium

  * Rebuilt against Ruby 2.7.
  * debian/ruby-tests.rake:
    - Rename to ruby-tests.rb.

 -- Georg Faerber <georg@debian.org>  Wed, 05 Feb 2020 13:29:11 +0000

ruby-gpgme (2.0.19-2) unstable; urgency=medium

  * debian/control:
    - Run wrap-and-sort.
    - Bump Standards-Version to 4.5.0, no changes required.
  * debian/patches:
    - Add patch to remove useless test dependencies.
    - Add patch to skip failing tests. It seems, some are broken if system
      libraries are in use, like in our case.
  * debian/ruby-tests.rb:
    - Fix to run upstream tests during package build. (Closes: #941950)

 -- Georg Faerber <georg@debian.org>  Tue, 04 Feb 2020 17:34:43 +0000

ruby-gpgme (2.0.19-1) unstable; urgency=medium

  * New upstream version 2.0.19:
    - Adds methods to do minimal key export and to check for key validity.
  * debian/control:
    - Bump debhelper compat level to 12.
    - Bump Standards-Version to 4.4.1.
    - Use my debian.org email address.
  * debian/salsa-ci.yml:
    - Move CI config to debian/salsa-ci.yml according to team policy.
    - Disable reprotest job, as it leads to false positives.

 -- Georg Faerber <georg@debian.org>  Mon, 07 Oct 2019 20:48:08 +0000

ruby-gpgme (2.0.18-1) unstable; urgency=medium

  * New upstream release.
  * debian/compat:
    - Drop obsolete compat file.
  * debian/control:
    - Use Alioth mailing list in Maintainer field.
    - Add build dependency on debhelper-compat.
    - Bump Standards-Version to 4.2.1.
    - Declare that the build doesn't need root privileges.
    - Wrap and sort.
  * debian/copyright:
    - Wrap and sort.
  * debian/patches:
    - Refresh for new release.
  * debian/rules:
    - Make the build verbose.
  * debian/watch:
    - Use version 4 and pull releases from GitHub. The current gem file
      contains a gemspec with non-ASCII characters, which leads to fatal quilt
      errors.
  * debian/.gitlab-ci.yml:
    - Introduce Salsa CI config.

 -- Georg Faerber <georg@riseup.net>  Sun, 16 Dec 2018 17:38:09 +0000

ruby-gpgme (2.0.16-1) unstable; urgency=medium

  * New upstream release.
  * debian/changelog: Remove trailing whitespace.
  * debian/control:
    - Bump Standards-Version to 4.1.3, no changes needed.
    - Use salsa.debian.org in Vcs-Browser and Vcs-Git fields, as
      anonscm.debian.org is deprecated and accordingly, all repositories were
      moved.
    - Use debian-ruby@lists.debian.org for Maintainer field.
  * debian/copyright: Bump years to include 2018.
  * debian/patches: Refresh for release 2.0.16-1.

 -- Georg Faerber <georg@riseup.net>  Wed, 07 Feb 2018 22:37:24 +0100

ruby-gpgme (2.0.14-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Depend on libgpgme-dev, instead of the obsolete
    libgpgme11-dev.
  * debian/copyright:
    - Bump years and add missing Debian packaging authors.
    - Use https in links.

 -- Georg Faerber <georg@riseup.net>  Tue, 07 Nov 2017 15:38:02 +0100

ruby-gpgme (2.0.13-1) unstable; urgency=medium

  * New upstream release.
  * debian/compat: Bump debhelper compat to 10.
  * debian/control:
    - Bump Standards-Version to 4.1.1, no changes needed.
    - Bump required debhelper version.
    - Add myself as uploader.
    - Use https in homepage link.
  * debian/patches: Refresh for release 2.0.13-1.
  * debian/watch: Use https in gemwatch link.

 -- Georg Faerber <georg@riseup.net>  Mon, 23 Oct 2017 13:21:00 +0200

ruby-gpgme (2.0.12-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Christian Hofstaedtler ]
  * New upstream version.
  * Update packaging using dh-make-ruby -w

 -- Christian Hofstaedtler <zeha@debian.org>  Sat, 05 Mar 2016 03:25:56 +0100

ruby-gpgme (2.0.9-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 2.0.9
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 18 Aug 2015 20:02:10 -0300

ruby-gpgme (2.0.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Increased Standards-Version to 3.9.5 (no changes).
  * Removed obsolete patch for dynamic build (with gpgme-config) and use
    --use-system-libraries option instead.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Tue, 29 Apr 2014 22:54:11 +0200

ruby-gpgme (2.0.2-1) unstable; urgency=low

  * Team upload.

  [ Gunnar Wolf ]
  * Removed the build of transitional packages and obsolete
    relations required for Wheezy.

  [ Marc Dequènes (Duck) ]
  * New upstream release.
  * Patched buildsys to link with gpgme dynamically (and not with
    embedded libraries).
  * Increased Standards-Version to 3.9.4 (no changes).
  * Added dpkg-source local option to unapply patches.

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Mon, 12 Aug 2013 20:01:08 +0200

ruby-gpgme (2.0.0-2) unstable; urgency=low

  * Added myself as uploader.
  * Build depend on gem2deb >= 0.3.0.
  * Bump standards version to 3.9.3 (no changes needed).
  * Use copyright format 1.0 uri.
  * Changed short description.
    - Remove lintian warning.

  [ Vincent Fourmond ]
  * Moved transitional packages to Priority: extra

 -- Per Andersson <avtobiff@gmail.com>  Mon, 25 Jun 2012 23:37:48 +0200

ruby-gpgme (2.0.0-1) unstable; urgency=low

  * Repackaged using the Gem2deb infrastructure

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 07 Oct 2011 12:11:01 -0500

libgpgme-ruby (1.0.8-3) unstable; urgency=low

  * Adopted package (Closes: #572175)
  * Dropped depensimple-patchsys from build process (no patches to
    apply)
  * Standards-version 3.8.1→3.8.4 (no changes needed)

 -- Gunnar Wolf <gwolf@debian.org>  Thu, 04 Mar 2010 07:56:54 -0600

libgpgme-ruby (1.0.8-2) unstable; urgency=low

  * Switch from Ruby 1.9 to 1.9.1. Closes: #565845.
  * Update debian/copyright. Closes: #538228.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Sat, 13 Feb 2010 10:43:25 +0100

libgpgme-ruby (1.0.8-1) unstable; urgency=medium

  * New upstream release (Closes: #563380)

 -- Micah Anderson <micah@debian.org>  Fri, 05 Feb 2010 13:54:48 -0500

libgpgme-ruby (1.0.6-1) unstable; urgency=low

  * New upstream release.

 -- Rudi Cilibrasi <cilibrar@debian.org>  Tue, 09 Jun 2009 07:05:56 -0700

libgpgme-ruby (1.0.5-1) unstable; urgency=low

  * [Gunnar Wolf]
    - Changed section to Ruby as per ftp-masters' request.
  * [Rudi Cilibrasi]
    - New upstream release.

 -- Rudi Cilibrasi <cilibrar@debian.org>  Wed, 03 Jun 2009 18:30:02 -0700

libgpgme-ruby (1.0.4-1) unstable; urgency=low

  * New upstream release.

 -- Rudi Cilibrasi <cilibrar@debian.org>  Tue, 10 Feb 2009 04:35:22 -0800

libgpgme-ruby (1.0.1-2) unstable; urgency=low

  * Build-depend on ruby-pkg-tools >= 0.14, than fixes the ruby1.9 libs
    install problem (see #484611).

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Mon, 16 Jun 2008 15:06:45 +0200

libgpgme-ruby (1.0.1-1) unstable; urgency=low

  * New upstream release.  Removed patch for memleak fixed by upstream.

 -- Rudi Cilibrasi <cilibrar@debian.org>  Thu, 06 Mar 2008 10:44:42 -0800

libgpgme-ruby (1.0.0-3) unstable; urgency=low

  [ Rudi Cilibrasi ]
  * Bumped Standards-Version to 3.7.3.

  * Thanks to Paul van Tilburg for help in packaging libgpgme-ruby.

  [ Lucas Nussbaum ]
  * Use new Homepage dpkg header.

 -- Rudi Cilibrasi <cilibrar@debian.org>  Fri, 28 Dec 2007 23:18:40 -0800

libgpgme-ruby (1.0.0-2) unstable; urgency=low

  * Thanks to Andreas Barth for fixing broken compilation problem.

 -- Rudi Cilibrasi <cilibrar@cilibrar.com>  Tue, 23 Oct 2007 14:55:35 -0700

libgpgme-ruby (1.0.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS, thanks Michael Ablassmeier. Closes: #427450

 -- Andreas Barth <aba@not.so.argh.org>  Mon,  2 Jul 2007 20:27:44 +0000

libgpgme-ruby (1.0.0-1) unstable; urgency=low

  [ Rudi Cilibrasi ]
  * I am taking stewardship of the package for now as maintainer.
  * New upstream version.
  * Changed Build-Deps to specify libgpgme11-dev.
  * Updated Standards to 3.7.2.

  [ Paul van Tilburg ]
  * Updated homepage URL in debian/control and debian/watch,
    since the project moved to RubyForge.
  * Added myself to the Uploaders, currently acting as sponsor.
  * Adapted debian/rules, and removed debian/control.in to drop
    the Uploaders rule.

 -- Paul van Tilburg <paulvt@debian.org>  Wed, 18 Apr 2007 22:30:24 +0200

libgpgme-ruby (0.2-2) unstable; urgency=low

  [ Thierry Reding ]
  * Set myself as maintainer, taking 'ownership' of the package.
  * Updated debian/control.in to use @RUBY_EXTRAS_TEAM@ instead of the
    deprecated @RUBY_TEAM@ (build-depends on ruby-pkg-tools >= 0.8).

  [ Paul van Tilburg ]
  * Added debian/watch file.

 -- Paul van Tilburg <paulvt@debian.org>  Thu, 30 Mar 2006 13:49:20 +0200

libgpgme-ruby (0.2-1) unstable; urgency=low

  * Initial release. (Closes: #351270)
  * Uses the new extconf.rb CDBS class (depends on ruby-pkg-tools >= 0.7).

 -- Thierry Reding <thierry@doppeltgemoppelt.de>  Fri, 24 Feb 2006 19:04:34 +0100
